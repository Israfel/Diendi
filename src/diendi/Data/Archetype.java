/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import diendi.Data.Globals.SpellList;

/**
 * Basic object to a representation of an archetype.
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class Archetype {

    /**
     * Unique ID of the Archetype
     */
    private int ID;
    /**
     * Textual name of the archetype
     */
    private String Name;
    /**
     * Map of all the spells associated with this Archetype
     */
    private Map<Integer, ArrayList<Integer>> Spells;
    //TODO: Possibly add an Archetype description

    /**
     * Creates a skeleton of an Archetype with no spells associated with it.
     *
     * @param id The unique ID for the Archetype
     * @param nm The name of the Archetype
     */
    public Archetype(int id, String nm) {
        ID = id;
        Name = nm;
        Spells = new HashMap();
    }

    /**
     * Creates an Archetype with the spells associated with it.
     *
     * @param id The unique ID for the Archetype
     * @param nm The name of the Archetype
     * @param sp An ArrayList of spell IDs to be associated with the Archetype
     */
    public Archetype(int id, String nm, ArrayList<Integer> sp) {
        ID = id;
        Name = nm;
        Spells = new HashMap();
        for (Integer i : sp) {
            if (!SpellList.spellPresent(i)) {
                System.out.println("Spell with ID " + i + " does not exist in Spell List");
                Spells = null;
                return;
            }
            Spell spell = SpellList.getSpell(i);
            if (verifyLevel(spell.getLevel(), true)) {
                Spells.get(spell.getLevel()).add(spell.getID());
            }
        }
    }

    /**
     * Adds a spell to the Archetype's spell list.
     *
     * @param id The ID of the spell to add
     * @param lv The level of the spell to add
     */
    public void addSpell(int id, int lv) {
        verifyLevel(lv, true);
        Spells.get(lv).add(id);
    }

    /**
     * Adds a spell to the Archetype's spell list. More costly than addSpell(int
     * id, int lv), should only be used when level not known.
     *
     * @param id The ID of the spell to add
     */
    public void addSpell(int id) {
        Spell tmp = SpellList.getSpell(id);
        verifyLevel(tmp.getLevel(), true);
        Spells.get(tmp.getLevel()).add(tmp.getID());
    }

    /**
     * Convert the object into an easy to read format
     *
     * @return A string in the format of {ID}: [Name|Spells]
     */
    @Override
    public String toString() {
        String ret = "<Archetype>{" + ID + "}: [" + Name + "|" + Spells.toString() + "]";
        return ret;
    }

    /*
     * Private Functions
     */
    /**
     * Verifies that the spell level exists within the spell map.
     *
     * @param lv The level of spell to check.
     * @param create If set to true, the level will be created if it does not
     * exist.
     * @return True if the spell level exists, false if it does not.
     */
    private boolean verifyLevel(int lv, boolean create) {
        if (Spells.get(lv) == null) {
            if (create) {
                Spells.put(lv, new ArrayList());
            } else {
                return false;
            }
        }
        return true;
    }

}
