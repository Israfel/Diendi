/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class CastingRequirements {

    /**
     * Defines the three types of casting requirements: V S M
     */
    static public enum CastingType {
        VERBAL,
        SOMATIC,
        MATERIAL
    };

    /**
     * The types contained within this CastingRequirements object.
     */
    CastingType[] Types;

    /**
     * The material components associated with this CastingRequirements object.
     * Set to null if no materials are associated.
     */
    String MaterialComp = null;

    public CastingRequirements(CastingType[] t, String mc) {
        Types = t;
        if (!hasMaterial(t)) {
            return;
        }
        MaterialComp = mc;
    }

    public CastingRequirements(CastingType[] t) {
        Types = t;
        if (!hasMaterial(t)) {
            System.out.println("Warning! Has casting type m but no material!");
            return;
        }
    }

    /**
     * Convert the object into an easy to read format
     *
     * @return A string in the format of {ID}: [types|materials]
     */
    @Override
    public String toString() {
        String ret = "<CastingRequirements>[";
        boolean hasMat = false;
        for (CastingType t : Types) {
            if (t.equals(CastingType.VERBAL)) {
                ret += "v";
            } else if (t.equals(CastingType.SOMATIC)) {
                ret += "s";
            } else if (t.equals(CastingType.MATERIAL)) {
                ret += "m";
                hasMat = true;
            }
        }
        if (hasMat) {
            ret += "|" + MaterialComp + "]";
        } else {
            ret += "]";
        }
        return ret;
    }

    public String toSpellBookString() {
        String ret = "";
        boolean hasMat = false;
        for (CastingType t : Types) {
            if (t.equals(CastingType.VERBAL)) {
                ret += "V";
            } else if (t.equals(CastingType.SOMATIC)) {
                ret += "S";
            } else if (t.equals(CastingType.MATERIAL)) {
                ret += "M";
                hasMat = true;
            }
        }
        if (hasMat) {
            ret += "|" + MaterialComp;
        }
        return ret;
    }

    /**
     * Tests to see if the casting type array contains MATERIAL as one of the
     * types.
     *
     * @param t Array of CastingTypes
     * @return True if the array contains MATERIAL, false otherwise.
     */
    private boolean hasMaterial(CastingType[] t) {
        for (CastingType ct : Types) {
            if (ct.equals(CastingType.MATERIAL)) {
                return true;
            }
        }
        return false;
    }
}
