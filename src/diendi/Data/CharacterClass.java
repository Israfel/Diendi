/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class CharacterClass {

    /**
     * Textual name of the class
     */
    private String Name;
    /**
     * Unique ID of the class
     */
    private int ID;
    /**
     * The Archetypes that are associated with this class
     */
    private ArrayList<Archetype> Archetypes;
    /**
     * A map of all spells known by this class based on spell level
     */
    private Map<Integer, ArrayList<Integer>> Spells;

    /**
     * Basic constructor for a CharacterClass, includes no Spells or Archetypes
     *
     * @param id ID for the CharacterClass
     * @param nm Name for the CharacterClass
     */
    public CharacterClass(int id, String nm) {
        ID = id;
        Name = nm;
    }

    /**
     * Creates a fully realized CharacterClass object.
     *
     * @param id The ID for the CharacterClass
     * @param nm The Name for the CharacterClass
     * @param at An ArrayList of archetypes to be included in the CharacterClass
     * @param sp An ArrayList of the IDs for spells to be included in the
     * CharacterClass.
     */
    public CharacterClass(int id, String nm, ArrayList<Archetype> at, ArrayList<Integer> sp) {
        ID = id;
        Name = nm;
        Archetypes = at;
        //TODO: Add spells to map
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    /**
     * Adds Archetype to the Archetypes list. If the Archetype is already
     * present, does nothing.
     *
     * @param at Archetype to add
     */
    public void addArchetype(Archetype at) {
        if (Archetypes.contains(at)) {
            System.out.println("Archetype " + at + " already added to class " + ID);
            return;
        }
        Archetypes.add(at);
    }

    /**
     * Removes Archetype from the Archetypes list. If the Archetype is not
     * present, does nothing.
     *
     * @param at Archetype to Remove
     */
    public void removeArchetype(Archetype at) {
        if (!Archetypes.contains(at)) {
            System.out.println("Archetype " + at + " not in Archtypes for class " + ID);
            return;
        }
        Archetypes.remove(at);
    }

    /**
     * Convert the object into an easy to read format
     *
     * @return A string in the format of {ID}: [Name|Archetypes|Spells]
     */
    @Override
    public String toString() {
        String ret = "<CharacterClass>{" + ID + "}: [" + Name + "|" + Archetypes.toString() + "|" + Spells.toString() + "]";
        return ret;
    }
}
