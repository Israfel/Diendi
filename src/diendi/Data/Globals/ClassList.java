/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data.Globals;

import java.util.HashMap;
import java.util.Map;
import diendi.Data.CharacterClass;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
/**
 * Static class to hold a master list of all classes that have been loaded into
 * the system.
 */
public class ClassList {

    /**
     * Map containing a global listing of all classes currently loaded into the
     * application.
     */
    private static Map<Integer, CharacterClass> ClassList = new HashMap<Integer, CharacterClass>();

    /**
     * Add a CharacterClass to the global class list.
     *
     * @param newClass The class to be added to the master listing.
     */
    public static void addClass(CharacterClass newClass) {
        if (ClassList.containsKey(newClass.getID())) {
            System.out.println("A class with key " + newClass.getID() + " already exists.");
            return;
        }

        ClassList.put(newClass.getID(), newClass);
    }

    /**
     * Remove a class from the global CharacterClass list based on it's ID.
     *
     * @param ID The ID of the CharacterClass to remove.
     */
    public static void removeClass(int ID) {
    }

    /**
     * Remove a class from the global CharacterClass list based on the full
     * object.
     *
     * @param removeClass The CharacterClass to remove
     */
    public static void removeClass(CharacterClass removeClass) {
    }

    /**
     * Determines if a CharacterClass with the matching ID is present in the
     * master list.
     *
     * @param ID The ID to check for
     * @return True if a CharacterClass is present matching the ID, false if
     * there is no matching ID present.
     */
    public static boolean classPresent(int ID) {
        if (ClassList.containsKey(ID)) {
            return true;
        }
        return false;
    }

    /**
     * Determines if a CharacterClass is present in the master list.
     *
     * @param charClass The CharacterClass to check for
     * @return True if the CharacterClass is present in the master list, false
     * if it is not present.
     */
    public static boolean classPresent(CharacterClass charClass) {
        if (ClassList.containsKey(charClass.getID())) {
            return true;
        }
        return false;
    }

    /**
     * Get a CharacterClass object from the master list based on it's ID.
     *
     * @param ID The ID to check for
     * @return The CharacterClass that matches the ID provided or null if no
     * matching object is found
     */
    public static CharacterClass getClass(int ID) {
        if (ClassList.containsKey(ID)) {
            return ClassList.get(ID);
        }
        return null;
    }

    public static int getClassSize() {
        return ClassList.size();
    }
}
