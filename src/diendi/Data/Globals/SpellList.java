/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data.Globals;

import java.util.HashMap;
import java.util.Map;
import diendi.Data.Spell;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
/**
 * Static class to hold a master list of all spells that have been loaded into
 * the system.
 *
 */
public class SpellList {

    /**
     * Map holding all spells in one place in memory. All other references to
     * spells should be done with IDs and this Map queried to get the actual
     * contents of the spell.
     */
    static private Map<Integer, Spell> spellList = new HashMap<Integer, Spell>();

    /**
     * Initialize the spellList, call before making any use of this class.
     */
//    @Deprecated
//    static public void initSpellList(){
//     spellList = new HashMap();   
//    }
    /**
     * This function is used to retrieve a spell from the master SpellList.
     *
     * @param id The ID of the spell to retrieve.
     * @return The spell that corresponds with the given ID, or null if that
     * spell is not present in the SpellList.
     */
    static public Spell getSpell(int id) {
        if (spellPresent(id)) {
            return spellList.get(id);
        }
        return null;
    }

    /**
     * Add a spell to the spell list.
     *
     * @param sp The spell to add
     */
    static public void addSpell(Spell sp) {
        if (spellPresent(sp.getID())) {
            return;
        }
        spellList.put(sp.getID(), sp);
    }

    /**
     * Remove a spell from the spell list
     *
     * @param sp tThe spell to remove
     */
    static public void removeSpell(Spell sp) {
        if (!spellPresent(sp.getID())) {
            return;
        }
        spellList.remove(sp.getID());
    }

    /**
     * Remove a spell from the spell list based on it's ID.
     *
     * @param id ID of the spell to remove.
     */
    static public void removeSpell(int id) {
        if (spellList.containsKey(id)) {
            spellList.remove(id);
            return;
        }
    }

    /**
     * Verify if a spell is in the spell list or not.
     *
     * @param id ID of the spell to verify
     * @return True if the spell is present in the spell list, false if it is
     * not.
     */
    static public boolean spellPresent(int id) {
        return spellList.containsKey(id);
    }
}
