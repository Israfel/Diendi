/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data.Globals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static diendi.Data.Globals.SpellList.getSpell;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class SpellLookUpList {

    /*
     * Creates a Spell look up list with an integer to represent the class id
     * and an arraylist filled with ids of spells corresponding to the class id
     */
    static private HashMap<Integer, ArrayList<Integer>> spellLookUpList = new HashMap<Integer, ArrayList<Integer>>();

    /**
     * Adds a spell id to its corresponding class array list i.e. "Fly" added to
     * warlock ArrayList in spellLookUpList HashMap
     */
    static public void addSpellId(int classId, int spellId) {
        /*
         * Checks if spell id is present in the array list
         */
        if (spellIdPresent(classId, spellId)) {
            return;
        }

        /**
         * Creates temporary ArrayList to copy old spell lookup ArrayList, adds
         * new spell id, and updates the spell lookup array list
         */
        ArrayList<Integer> idList = (spellLookUpList.get(classId));

        if (idList == null) {
            idList = new ArrayList<Integer>();
            idList.add(spellId);
            spellLookUpList.put(classId, idList);
        } else {
            idList.add(spellId);
            spellLookUpList.put(classId, idList);
        }
    }

    /*
     * Checks if a spell id is present in a classes ArrayList
     */
    static public boolean spellIdPresent(int classId, int spellId) {
        if (spellLookUpList.containsValue(classId)) {
            return spellLookUpList.get(classId).contains(spellId);
        }

        return false;
    }

    /*
     * Returns all DnD class spell ids in an ArrayList
     */
    static public ArrayList getAllClassSpellIds(int classId) {
        return spellLookUpList.get(classId);
    }

    /*
     * Returns all desired level spells from specific class in and ArrayList
     */
    static public ArrayList getLvlClassSpellIds(int classId, int spellLvl) {
        ArrayList<Integer> classSpells = spellLookUpList.get(classId);
        ArrayList<Integer> classLvlSpells = new ArrayList();

        /*
         * Iterates through spell id list to find and remove any spells that are
         * not the desired level Note: more efficient to create second arraylist
         * and store desired spells instead?
         */
        for (int i = 0; i < classSpells.size(); i++) {
            if (getSpell(classSpells.get(i)).getLevel() == spellLvl) {
                classLvlSpells.add(classSpells.get(i));
            }
        }

        return classLvlSpells;
    }

}
