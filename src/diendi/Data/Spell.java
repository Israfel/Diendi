/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.Data;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
/**
 * Basic object for holding the definition of a spell
 */
public class Spell {

    /**
     * The unique ID of the spell, this is used to link a spell to a specific
     * class.
     */
    private int ID;
    /**
     * The casting level of the spell. This can range from 0(Cantrip) to 9.
     */
    private int Level;
    /**
     * The actual name of the spell.
     */
    private String Name;
    /**
     * A CastingRequirements object representing the form of casting required,
     * as well as the material components required, if any.
     */
    private CastingRequirements Reqs;
    /**
     * The actual textual description of the effects of the spell.
     */
    private String Description;

    /**
     * Create a skeleton instance of a spell with only the bare bones components
     * filled in.
     *
     * @param id The unique ID of the spell to be created
     * @param lv The level (0-9) for the spell
     * @param nm The name of the spell
     */
    public Spell(int id, int lv, String nm) {
        ID = id;
        Level = lv;
        Name = nm;
        Reqs = null;
        Description = null;
    }

    /**
     * Full constructor for a Spell. This constructor creates a complete spell
     * object.
     *
     * @param id The unique ID of the spell to be created
     * @param lv The level (0-9) for the spell
     * @param nm The name of the spell
     * @param rq A CastingRequirements object that represents the casting
     * requirements of the spell.
     * @param dsc The text based description of the spell's effect.
     */
    public Spell(int id, int lv, String nm, CastingRequirements rq, String dsc) {
        ID = id;
        Level = lv;
        Name = nm;
        Reqs = rq;
        Description = dsc;
    }

    /**
     * Convert the object into an easy to read format
     *
     * @return A string in the format of {ID}:
     * [Name|Level|Requirements|"Description"]
     */
    @Override
    public String toString() {
        String ret = "<Spell>{" + ID + "}: [" + Name + "|Lvl" + Level + "|" + Reqs.toString() + "|\"" + Description + "\"]";
        return ret;
    }

    public String toSpellBookString() {
        String ret = Name + "\nLvl: " + Level + "\nComponents: " + Reqs.toSpellBookString() + "\nDescription:\n\n" + Description;
        return ret;
    }

    /**
     * Getter function for ID
     *
     * @return The ID of the spell.
     */
    public int getID() {
        return ID;
    }

    /**
     * Getter function for Level
     *
     * @return The level of the spell
     */
    public int getLevel() {
        return Level;
    }

    /**
     * Getter function for Name
     *
     * @return The name of the spell
     */
    public String getName() {
        return Name;
    }

    /**
     * Getter function for Description
     *
     * @return The description of the spell's effects.
     */
    public String getDesc() {
        return Description;
    }

    /**
     * Getter function for casting requirements
     *
     * @return The CastingRequirements object for the spell.
     */
    public CastingRequirements getReqs() {
        return Reqs;
    }
}
