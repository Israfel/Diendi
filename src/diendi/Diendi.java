/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import diendi.Data.CastingRequirements;
import diendi.Data.CastingRequirements.CastingType;
import diendi.Data.CharacterClass;
import diendi.Data.Globals.ClassList;
import diendi.Data.Globals.SpellList;
import diendi.Data.Globals.SpellLookUpList;
import diendi.Data.Spell;
import diendi.View.Controller.Diendi_MainController;
import java.io.InputStream;
import static javafx.application.Application.launch;

/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class Diendi extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/FXML/Diendi_Main.fxml"));
        Parent root = loader.load();

        Diendi_MainController spellController = loader.getController();

        Scene scene = new Scene(root);

        stage.setTitle("Diendi Spellbook");
        stage.setScene(scene);
        stage.show();

        spellController.setData();
    }

    public static void main(String[] args) throws IOException {

        CharacterClass nil = new CharacterClass(0, "Select a Class");
        CharacterClass bard = new CharacterClass(1, "Bard");
        CharacterClass cleric = new CharacterClass(2, "Cleric");
        CharacterClass druid = new CharacterClass(3, "Druid");
        CharacterClass paladin = new CharacterClass(4, "Paladin");
        CharacterClass ranger = new CharacterClass(5, "Ranger");
        CharacterClass sorcerer = new CharacterClass(6, "Sorceror");
        CharacterClass warlock = new CharacterClass(7, "Warlock");
        CharacterClass wizard = new CharacterClass(8, "Wizard");

        ClassList.addClass(nil);
        ClassList.addClass(bard);
        ClassList.addClass(cleric);
        ClassList.addClass(druid);
        ClassList.addClass(paladin);
        ClassList.addClass(ranger);
        ClassList.addClass(sorcerer);
        ClassList.addClass(warlock);
        ClassList.addClass(wizard);
        
        SAXBuilder builder = new SAXBuilder();
        InputStream xmlFile = Diendi.class.getResourceAsStream("MasterSpellList.xml");

        try {

            Document document = (Document) builder.build(xmlFile);
            Element rootNode = document.getRootElement();

            /*
             * Returns all of a spells attributes, name, level, etc. from XML
             */
            List list = rootNode.getChildren("spell");

            for (int i = 0; i < list.size(); i++) {

                /*
                 * Spell prototypes to be filled in from xml and used for new
                 * spell creation
                 */
                int spellLevel = -1;
                String spellName = "";
                String spellDesc = "";
                String spellMat = "";
                List<CastingType> spellReq = new ArrayList<CastingType>();

                Element spellNode = (Element) list.get(i);

                spellName = spellNode.getChildText("name");
                spellLevel = Integer.parseInt(spellNode.getChildText("level"));

                /*
                 * Returns "requirements" children
                 */
                Element reqNode = spellNode.getChild("requirements");

                if ("".equals(reqNode.getChildText("verbal"))) {
                    spellReq.add(CastingRequirements.CastingType.VERBAL);
                }

                if ("".equals(reqNode.getChildText("somatic"))) {
                    spellReq.add(CastingRequirements.CastingType.SOMATIC);
                }

                if (!("".equals(reqNode.getChildText("material")))) {
                    spellReq.add(CastingRequirements.CastingType.MATERIAL);
                    spellMat = reqNode.getChildText("material");
                }

                /*
                 * Returns "classes" children, indicating which classes this
                 * spell belongs to
                 */
                Element classesNode = spellNode.getChild("classes");

                spellDesc = spellNode.getChildText("description");

                /*
                 * Only prints Higher level description if present
                 */
                if (!(spellNode.getChildText("higherlevels").equals(""))) {
                    spellDesc += "\n\nAt Higher Levels: \n" + spellNode.getChildText("higherlevels");
                }

                /*
                 * Takes all variables taken from XML SpellLookUpList and
                 * creates a spell
                 */
                CastingRequirements reqs = new CastingRequirements(spellReq.toArray(new CastingRequirements.CastingType[0]), spellMat);
                Spell newSpell = new Spell(i, spellLevel, spellName, reqs, spellDesc);
                SpellList.addSpell(newSpell);

                /*
                 * Adds the spell to the appropriate DnD class based on class
                 * Ids from a list of Ids
                 */
                List classes = classesNode.getChildren("class");

                for (int j = 0; j < classes.size(); j++) {
                    Element classNode = (Element) classes.get(j);
                    SpellLookUpList.addSpellId(Integer.parseInt(classNode.getChildText("id")), newSpell.getID());
                }

            }

        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }

        launch(args);
    }

}
