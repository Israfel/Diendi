/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diendi.View.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import diendi.Data.Globals.ClassList;
import diendi.Data.Globals.SpellList;
import diendi.Data.Globals.SpellLookUpList;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
/**
 *
 * @author Alfredo Sanchez <Alfredo.Sanchez.CS@gmail.com>
 */
public class Diendi_MainController implements Initializable {

    @FXML
    private GridPane SpellMenu;
    @FXML
    private ChoiceBox classList;
    @FXML
    private TextArea SpellListView;
    @FXML
    private CheckBox cb0;
    @FXML
    private CheckBox cb1;
    @FXML
    private CheckBox cb2;
    @FXML
    private CheckBox cb3;
    @FXML
    private CheckBox cb4;
    @FXML
    private CheckBox cb5;
    @FXML
    private CheckBox cb6;
    @FXML
    private CheckBox cb7;
    @FXML
    private CheckBox cb8;
    @FXML
    private CheckBox cb9;

    private static List spellLevels = new ArrayList();

    /*
     * Prepares Choicebox and fills it with DnD classes
     */
    public void setData() {

        classList.getItems().clear();

        /*
         * Adds dummy class "Select a Class" and sets it as default in the
         * ChoiceBox
         */
        classList.getItems().addAll(ClassList.getClass(0).getName());
        classList.setValue(ClassList.getClass(0).getName());

        /*
         * Adds all DnD classes to the ChoiceBox
         */
        for (int i = 1; i < ClassList.getClassSize(); i++) {
            classList.getItems().addAll(ClassList.getClass(i).getName());
        }
    }

    /*
     * Uses appendText() to display spells in SpellListView based on character
     * class type or spell level
     */
    private void displaySpells(int classId) {
        /*
         * Imports custom monospaced font and loads it
         */
            Font.loadFont(getClass().getClassLoader().getResourceAsStream("resources/FiraMono-Regular.ttf"), 12);
        /*
         * Displays all spells if no spell level checkbox is selected
         */
        if (classId != 0 && !spellLevels.contains(Boolean.TRUE)) {
            SpellListView.clear();
            SpellListView.setFont(Font.font("Fira Mono", 12));
            List<Integer> idList = (SpellLookUpList.getAllClassSpellIds(classId));
            for (int i = 0; i < idList.size(); i++) {
                SpellListView.appendText(SpellList.getSpell(idList.get(i)).toSpellBookString() + "\n-------------------------------------------------------------------------------\n");
            }
            SpellListView.positionCaret(0);

        } /*
         * Displays spells based on selected spell level checkbox
         */ else if (classId != 0) {
            SpellListView.clear();
            SpellListView.setFont(Font.font("Fira Mono", 12));
            List<Integer> idList = new ArrayList();
            for (int i = 0; i <= 9; i++) {
                if (spellLevels.get(i) == Boolean.TRUE) {
                    idList.addAll(SpellLookUpList.getLvlClassSpellIds(classId, i));
                }
            }
            Collections.sort(idList);
            for (int i = 0; i < idList.size(); i++) {
                SpellListView.appendText(SpellList.getSpell(idList.get(i)).toSpellBookString() + "\n-------------------------------------------------------------------------------\n");
            }
            SpellListView.positionCaret(0);
        } /*
         * Welcome screen
         */ else {
            SpellListView.clear();
            SpellListView.setFont(Font.font("Fira Mono", 24));
            SpellListView.appendText("Welcome to the Diende Spellbook");

        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        SpellListView.setEditable(false);

        /*
         * Initializes a list with false booleans corresponding to the state of
         * the respective checkbox
         */
        for (int i = 0; i <= 9; i++) {
            spellLevels.add(Boolean.FALSE);
        }

        /*
         * Displays spells of the selected character class from dropdown menu
         */
        classList.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue ov, Number oldValue, Number newValue) {
                displaySpells(newValue.intValue());
            }
        });

        /*
         * From cb0 to cb9: Updates SpellListView with spells of the selected
         * spell level checkbox
         */
        cb0.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb0.getText()), cb0.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb1.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb1.getText()), cb1.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb2.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb2.getText()), cb2.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb3.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb3.getText()), cb3.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb4.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb4.getText()), cb4.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb5.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb5.getText()), cb5.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb6.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb6.getText()), cb6.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb7.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb7.getText()), cb7.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb8.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb8.getText()), cb8.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

        cb9.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                spellLevels.set(Integer.parseInt(cb9.getText()), cb9.isSelected());
                displaySpells(classList.getSelectionModel().getSelectedIndex());
            }
        });

    }

}
